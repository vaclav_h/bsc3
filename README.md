## Build:

``
mvn clean package
``

Run with default data packaged with the project:

``
java -jar bsc3.jar classes/default_data
``

## Validations:

User input validation errors are printed to the console and are considered as "soft" so the app keeps running.