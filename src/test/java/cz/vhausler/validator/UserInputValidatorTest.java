package cz.vhausler.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test suite for {@link UserInputValidator}.
 *
 * @author Václav Häusler <a href="mailto:vaclav.hausler@gmail.com">vaclav.hausler@gmail.com</a>
 */
class UserInputValidatorTest {

    private UserInputValidator validator;

    @BeforeEach
    private void init() {
        validator = new UserInputValidator();
    }

    @Test
    void testValidate() {
        String str = "3.4 08801";
        assertTrue(validator.validate(str).isEmpty());
    }

    @Test
    void testValidateTooManyDecimals() {
        String str = "3.4111 08801";
        List<String> validationErrors = validator.validate(str);
        assertThat(validationErrors).hasSize(1);
        assertEquals(validationErrors.get(0), UserInputValidator.MAX_WEIGHT_DECIMALS_ERR);
    }

    @Test
    void testValidateMinWeight() {
        String str = "-3.4 08801";
        List<String> validationErrors = validator.validate(str);
        assertThat(validationErrors).hasSize(1);
        assertEquals(validationErrors.get(0), UserInputValidator.MIN_WEIGHT_ERR);
    }

    @Test
    void testValidatePostalCodeDigits() {
        String str = "3.4 088010";
        List<String> validationErrors = validator.validate(str);
        assertThat(validationErrors).hasSize(1);
        assertEquals(validationErrors.get(0), UserInputValidator.POSTAL_CODE_DIGITS_ERR);

        str = "3.4 0abcd";
        validationErrors = validator.validate(str);
        assertThat(validationErrors).hasSize(1);
        assertEquals(validationErrors.get(0), UserInputValidator.POSTAL_CODE_DIGITS_ERR);
    }

    @Test
    void testValidateInvalidUserInput() {
        String str = "3.4_08801";
        List<String> validationErrors = validator.validate(str);
        assertThat(validationErrors).hasSize(1);
        assertEquals(validationErrors.get(0), UserInputValidator.INVALID_USER_INPUT_ERR);
    }

    @Test
    void testValidateTooManyArguments() {
        String str = "3.4 08801 5.32";
        List<String> validationErrors = validator.validate(str);
        assertThat(validationErrors).hasSize(1);
        assertEquals(validationErrors.get(0), UserInputValidator.TOO_MANY_ARGUMENTS_ERR);
    }

}