package cz.vhausler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.net.URL;

import cz.vhausler.model.PostalPackage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Test suite for {@link Main}.
 *
 * @author Václav Häusler <a href="mailto:vaclav.hausler@gmail.com">vaclav.hausler@gmail.com</a>
 */
class MainTest {

    private Main main;

    @BeforeEach
    private void init() {
        main = new Main();
    }

    @Test
    @Disabled
    void testMainKeepRunning() {
        URL defaultData = MainTest.class.getResource("default_test_data");
        // load the default data
        main.processArgs(new String[]{defaultData.getFile()});

        // simulate user input
        String userInput = "6.6666 08802";
        main.processUserInput(userInput);

        userInput = "6.6 08802";
        main.processUserInput(userInput);
    }

    @Test
    void testMain() {
        URL defaultData = getClass().getClassLoader().getResource("default_test_data");

        assertNotNull(defaultData);

        // load the default data
        main.processArgs(new String[]{defaultData.getFile()});

        // validate the default data
        assertThat(main.getPackages()).hasSize(5);
        assertThat(main.getPackages()).contains(new PostalPackage(new BigDecimal("3.4"), "08801"));
        assertThat(main.getPackages()).contains(new PostalPackage(new BigDecimal("2"), "90005"));
        assertThat(main.getPackages()).contains(new PostalPackage(new BigDecimal("12.56"), "08801"));
        assertThat(main.getPackages()).contains(new PostalPackage(new BigDecimal("5.5"), "08079"));
        assertThat(main.getPackages()).contains(new PostalPackage(new BigDecimal("3.2"), "09300"));

        // simulate user input
        String userInput = "6.6666 08802";
        main.processUserInput(userInput);

        userInput = "6.6 08802";
        main.processUserInput(userInput);

        userInput = "quit";
        main.processUserInput(userInput);

        // validate the newly added package
        assertThat(main.getPackages()).hasSize(6);
        assertThat(main.getPackages()).contains(new PostalPackage(new BigDecimal("6.6"), "08802"));
    }

    @Test
    void testMainIllegalArguments() {
        assertThrows(IllegalArgumentException.class, () -> main.processArgs(new String[]{}));
    }

    @Test
    void testMainMissingFile() {
        assertThrows(IllegalStateException.class, () -> main.processArgs(new String[]{"default_data"}));
    }
}