package cz.vhausler.model;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Model class for the package.
 *
 * @author Václav Häusler <a href="mailto:vaclav.hausler@gmail.com">vaclav.hausler@gmail.com</a>
 */
@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class PostalPackage {

    private BigDecimal weight;
    private String postalCode;

}
