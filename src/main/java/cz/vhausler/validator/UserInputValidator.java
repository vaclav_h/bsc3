package cz.vhausler.validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
 * Validates user inputs.
 *
 * @author Václav Häusler <a href="mailto:vaclav.hausler@gmail.com">vaclav.hausler@gmail.com</a>
 */
@Slf4j
public class UserInputValidator {

    // values
    private static final String USAGE = "<weight: positive number, >0, maximal 3 decimal places, . (dot) as decimal separator><space><postal code: " +
        "fixed 5 digits>";
    private static final int MAX_WEIGHT_DECIMALS = 3;
    private static final int POSTAL_CODE_DIGITS = 5;

    // errors
    static final String INVALID_USER_INPUT_ERR = "Usage: " + USAGE;
    static final String TOO_MANY_ARGUMENTS_ERR = "Too many arguments, usage: " + USAGE;
    static final String POSTAL_CODE_DIGITS_ERR = "Postal code is supposed to have " + POSTAL_CODE_DIGITS + " digits";
    static final String MAX_WEIGHT_DECIMALS_ERR = "More than " + MAX_WEIGHT_DECIMALS + " decimal places are not allowed for weight";
    static final String MIN_WEIGHT_ERR = "Weight has to be greater than 0";

    /**
     * Validates the user input. Validations:
     * <pre>
     * <ul>
     *     <li>user input contains space</li>
     *     <li>user input has the right amount of arguments</li>
     *     <li>weight is valid</li>
     *     <li>postal code is valid</li>
     * </ul>
     * </pre>
     *
     * @param str to be validated
     * @return list of validation errors
     */
    public List<String> validate(String str) {
        if (str == null || !str.contains(" ")) {
            log.debug("Invalid user input '{}'", str);
            return Collections.singletonList(INVALID_USER_INPUT_ERR);
        } else {
            String[] split = str.split(" ");
            if (split.length > 2) {
                log.debug("Too many arguments '{}'", str);
                return Collections.singletonList(TOO_MANY_ARGUMENTS_ERR);
            }
            List<String> validationErrors = new ArrayList<>();
            String weightStr = split[0];
            String postalCode = split[1];

            validationErrors.addAll(validateWeight(weightStr));
            validationErrors.addAll(validatePostalCode(postalCode));

            return validationErrors;
        }
    }

    /**
     * Validates the postal code and returns a list of validation errors.
     *
     * @param postalCode to be validated
     * @return list of validation errors
     */
    private List<String> validatePostalCode(String postalCode) {
        List<String> validationErrors = new ArrayList<>();
        if (postalCode.length() != 5 || !postalCode.matches("[0-9]+")) {
            log.debug("Invalid postal code '{}'", postalCode);
            validationErrors.add(POSTAL_CODE_DIGITS_ERR);
        }
        return validationErrors;
    }

    /**
     * Validates the weight and returns a list of validation errors.
     *
     * @param weightStr to be validated
     * @return list of validation errors
     */
    private List<String> validateWeight(String weightStr) {
        List<String> validationErrors = new ArrayList<>();
        if (weightStr != null && weightStr.length() > 0) {
            if (weightStr.contains(".") && weightStr.split("\\.")[1].length() > MAX_WEIGHT_DECIMALS) {
                log.debug("Max weight has too many decimals '{}'", weightStr);
                validationErrors.add(MAX_WEIGHT_DECIMALS_ERR);
            }
            if (weightStr.startsWith("-") || weightStr.startsWith("0")) {
                log.debug("Weight is bellow minimum allowed value '{}'", weightStr);
                validationErrors.add(MIN_WEIGHT_ERR);
            }
        }
        return validationErrors;
    }
}
