package cz.vhausler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cz.vhausler.model.PostalPackage;
import cz.vhausler.validator.UserInputValidator;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Main class of the application.
 *
 * @author Václav Häusler <a href="mailto:vaclav.hausler@gmail.com">vaclav.hausler@gmail.com</a>
 */
@Slf4j
public class Main {

    private UserInputValidator validator;
    private final static int TICK_LENGTH = 60 * 1000;

    @Getter
    private List<PostalPackage> packages;
    @Getter
    private Map<String, BigDecimal> totalPackageWeights;
    @Getter
    private boolean running = true;

    Main() {
        validator = new UserInputValidator();
        packages = new ArrayList<>();
        totalPackageWeights = new HashMap<>();
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.processArgs(args);

        main.printPackagesByTotalWeight();

        while (main.isRunning()) {
            main.processUserInput(main.call());
        }
    }

    /**
     * Processes user input.
     */
    void processUserInput(String input) {
        if (input != null) {
            switch (input) {
                case "quit":
                    stop();
                    break;
                default:
                    if (isUserInputValid(input)) {
                        processPackage(input);
                    }
                    break;
            }
        }
    }

    /**
     * Waits for user input, does not block threads like {@link java.util.Scanner}.
     *
     * @return user input
     */
    private String call() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input;
        do {
            try {
                long lastTick = System.currentTimeMillis();
                // wait until we have data to complete a readLine()
                while (!br.ready() && running) {
                    if (System.currentTimeMillis() - lastTick >= TICK_LENGTH) {
                        printPackagesByTotalWeight();
                        lastTick = System.currentTimeMillis();
                    }

                    Thread.sleep(200);
                }
                input = br.readLine();
            } catch (IOException | InterruptedException ex) {
                log.error("Unexpected exception {}", ex.getMessage());
                return null;
            }
        } while ("".equals(input));
        return input;
    }

    /**
     * Sorts and prints out the current packages.
     */
    private void printPackagesByTotalWeight() {
        LinkedHashMap<String, BigDecimal> sorted = totalPackageWeights.entrySet().stream()
                                                                      .sorted(Map.Entry.<String, BigDecimal>comparingByValue().reversed())
                                                                      .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                                                          (e1, e2) -> e1, LinkedHashMap::new));
        printMessage("################ PACKAGES ################");
        for (Map.Entry<String, BigDecimal> entry : sorted.entrySet()) {
            printMessage(entry.getKey() + " " + entry.getValue());
        }
    }

    /**
     * Parses the package from string and adds it to the list of packages. Doesn't validate the input string. For validation see
     * {@link UserInputValidator#validate(String)}.
     *
     * @param packageStr to be processed
     */
    private void processPackage(String packageStr) {
        String[] split = packageStr.split(" ");

        BigDecimal weight = new BigDecimal(split[0]);
        String postalCode = split[1];

        PostalPackage p = new PostalPackage(weight, postalCode);
        log.debug("Adding new package '{}'", p);
        packages.add(p);
        totalPackageWeights.put(postalCode, totalPackageWeights.getOrDefault(postalCode, new BigDecimal(0)).add(weight));
    }

    /**
     * Validates a single user input.
     *
     * @param str to be validated
     */
    private boolean isUserInputValid(String str) {
        log.debug("Validating user input {}", str);
        List<String> validationErrors = validator.validate(str);
        if (!validationErrors.isEmpty()) {
            validationErrors.forEach(this::printMessage);
            return false;
        }
        return true;
    }

    /**
     * Prints out the message for the user.
     *
     * @param msg to be printed
     */
    private void printMessage(String msg) {
        System.out.println(msg);
    }

    /**
     * Processes startup arguments.
     *
     * @param args to be processed
     */
    void processArgs(String[] args) {
        if (args.length == 0 || args.length > 1 || args[0] == null || args[0].length() == 0) {
            log.error("No filename received at startup");
            throw new IllegalArgumentException("Usage: <filename>");
        }

        String filename = args[0];
        File defaultData = new File(filename);
        try {
            processFile(new FileInputStream(defaultData));
        } catch (IOException ex) {
            log.error("Invalid filename received on startup '{}'", filename);
            throw new IllegalStateException("Default data file " + filename + " not found");
        }
    }

    /**
     * Loads the default data from the input stream.
     *
     * @param inputStream to be processed
     * @throws IOException if the stream could not be read
     */
    private void processFile(InputStream inputStream) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                processPackage(line);
            }
        }
    }

    /**
     * Stops the main program loop.
     */
    private void stop() {
        running = false;
        log.debug("Stopping the main application loop");
    }
}
